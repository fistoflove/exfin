<?php
/**
 * Child Starter functions and definitions
 *
 */
add_action('wp_head', function() {
	?>
	<link rel="icon" type="image/x-icon" href="/wp-content/themes/exfin/favicon.ico">
	<?php
  });

 add_action('wp_head', function() {
	$user = wp_get_current_user();
	if($user->exists()) {
		if(str_contains($user->user_email, '@imsmarketing.ie')) {
			?>
			<script>
			  window.markerConfig = {
				project: '63ebad3bec56d4dab58a6e69', 
				source: 'snippet',
				reporter: {
					email: 'vini@imsmarketing.ie',
					fullName: 'IMS Marketing',
				},
			  };
			</script>
			<?php
		} else {
			?>
			<script>
			  window.markerConfig = {
				project: '63ebad3bec56d4dab58a6e69', 
				source: 'snippet',
				reporter: {
					email: '<?= $user->user_email; ?>',
					fullName: '<?= $user->display_name; ?>',
				},
			  };
			</script>
			<?php
		}
		?>
		<script>
			!function(e,r,a){if(!e.__Marker){e.__Marker={};var t=[],n={__cs:t};["show","hide","isVisible","capture","cancelCapture","unload","reload","isExtensionInstalled","setReporter","setCustomData","on","off"].forEach(function(e){n[e]=function(){var r=Array.prototype.slice.call(arguments);r.unshift(e),t.push(r)}}),e.Marker=n;var s=r.createElement("script");s.async=1,s.src="https://edge.marker.io/latest/shim.js";var i=r.getElementsByTagName("script")[0];i.parentNode.insertBefore(s,i)}}(window,document);
		</script>
		<?php
	}
  });

add_action( 'login_enqueue_scripts', function() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/logo.svg);
			height: 185px;
			width: 285px;
			background-size: 285px 185px;
			background-repeat: no-repeat;
        }
    </style>
<?php });