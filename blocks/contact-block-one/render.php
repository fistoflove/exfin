<?php

use Timber\Timber;
use Timber\Post;
use IMSWP\Helper\Scss;
use IMSWP\Helper\Fields;

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;


$context['block_path']  = "/wp-content/themes/cec/blocks/become-a-host";


Timber::render( 'template.twig', $context);