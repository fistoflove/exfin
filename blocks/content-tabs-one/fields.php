<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Items", "repeater", [
            ["Title", "text"],
            ["Content", "text"],
            ["Image", "image"],
        ]],
    ]
);