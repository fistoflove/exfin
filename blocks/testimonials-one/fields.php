<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Items", "repeater", [
            ["Testimonial", "text"],
            ["Name", "text"],
            ["Company", "text"],
        ]],
    ]
);