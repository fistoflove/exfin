<?php

use Timber\Timber;
use Timber\Post;
use IMSWP\Helper\Helper;

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields(get_the_ID());

$context['is_preview'] = $is_preview;


Helper::output_this_block_css('single-case-study');

Helper::output_this_block_css('testimonials-one');

Helper::output_this_block_css('cta-block-one');

Timber::render( 'template.twig', $context);