<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Content", "wysiwyg"],
    ]
);

$fields->register_tab(
    "Details",
    [
        ["Find Us At", "wysiwyg"],
        ["Phone Us", "wysiwyg"],
        ["Email Us", "wysiwyg"],
    ]
);

$fields->register_tab(
    "CTA",
    [
        ["Title", "text"],
        ["Form", "text"],
    ]
);
