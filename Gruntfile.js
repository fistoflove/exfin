module.exports = function (grunt) {
  grunt.initConfig({
    sass: {
      options: {
        loadPath: ['dev/scss/theme'],
      },
      core: {
        files: {
          "static/base.css": "dev/scss/base.scss",
          "static/editor.css": "dev/scss/editor.scss",
        },
      },
      blocks: {
        files: [{
          expand: true,
          cwd: 'blocks/',
          src: ['**/style.scss'],
          dest: 'blocks/',
          ext: '.css',
        }]
      },
    },
    csso: {
      core: {
        options: {
          report: "gzip",
        },
        files: {
          "static/base.min.css": ["static/base.css"],
        },
      },
      blocks: {
        options: {
          report: "gzip",
        },
        files: [{
          expand: true,
          cwd: 'blocks/',
          src: ['**/style.css'],
          dest: 'blocks/',
          ext: '.min.css',
          extDot: 'first'
        }]
      },
    },
    watch: {
      files: [
        "dev/scss/*",
        "dev/scss/theme/*",
      ],
      tasks: [
        "sass:core",
        "csso:core",
      ],
    },
  });
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-csso");
  grunt.loadNpmTasks("grunt-contrib-watch");
};

